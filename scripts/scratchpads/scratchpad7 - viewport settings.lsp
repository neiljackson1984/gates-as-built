(C:UPDATE_EXTERNAL_BLOCK_DEFINITIONS )

;;======== COLLECT INFORMATION ABOUT THE ENTITIES IN PAPERSPACE ===============
(progn 
	(setq radian 1.0)
	(setq degree (/ (* Pi radian) 180.0))
	(setq inch 1.0)
	(setq millimeter (/ inch 25.4))
	(setq pixel (/ inch 300)) ;; this is just a guess - I have no idea what the Autocad 'acPixels' value of acPlotPaperUnits enum really means.

	(setq mainPViewport (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))))
	(reportOnPViewport mainPViewport)

	;; look for reportReference - the first block reference in paperspace whose effective name is "report"
	(setq reportReference nil)
	(vlax-for entity (vla-get-PaperSpace (vla-get-Document mainPViewport))
		(if 
			(and
				(not reportReference)
				(= "AcDbBlockReference" (vla-get-ObjectName entity))
				; (wcmatch  (vla-get-EffectiveName entity) "*-report")
				(wcmatch  (vla-get-EffectiveName entity) "report")
			)
			(progn
				(setq reportReference entity)
			)
		)
	)
	
   ;; look for titleBlockReference - the first block reference in paperspace whose effective name starts with "title_block"
	(setq titleBlockReference nil)
	(vlax-for entity (vla-get-PaperSpace (vla-get-Document mainPViewport))
		(if 
			(and
				(not titleBlockReference)
				(= "AcDbBlockReference" (vla-get-ObjectName entity))
				(wcmatch  (strcase (vla-get-EffectiveName entity)) (strcase "title_block*"))
			)
			(progn
				(setq titleBlockReference entity)
			)
		)
	)

	(setq labelBlockReference (getLabelBlockReferenceOfPViewport mainPViewport))
	(setq reportReferenceExtent (list 0 0 0))
	(if reportReference
		(progn
			(vla-GetBoundingBox reportReference 'minimumPoint 'maximumPoint)
			(setq minimumPoint (gc:VariantToLispData minimumPoint))
			(setq maximumPoint (gc:VariantToLispData maximumPoint))
			
			(setq reportReferenceExtentX (- (nth 0 maximumPoint) (nth 0 minimumPoint)))
			(setq reportReferenceExtentY (- (nth 1 maximumPoint) (nth 1 minimumPoint)))
			
			(setq reportReferenceExtent
				(list
					(- (nth 0 maximumPoint) (nth 0 minimumPoint))
					(- (nth 1 maximumPoint) (nth 1 minimumPoint))
					(- (nth 2 maximumPoint) (nth 2 minimumPoint))
				)
			)
		)
	)

	(princ "reportReferenceExtent: " )(princ reportReferenceExtent ) (princ "\n")(princ)

	(vlax-dump-object (vla-get-Layout (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object)))))

	(setq sheetWidth nil)
	(setq sheetHeight nil)
	(vla-GetPaperSize (vla-get-Layout (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object))))
		'sheetWidth
		'sheetHeight
	)

	;; from the manual's description the PaperUnits property: 
	;; This property determines the units for 
	;; the display of the layout or plot configuration in the user interface. This property does not 
	;; determine the units for input or query of the ActiveX Automation properties. All ActiveX 
	;; Automation properties are represented in millimeters or radians, regardless of the units settings. 

	;; from the manual's description the GetPaperSize method: 
	;; The units for the width and height values are specified by the PaperUnits property. 

	;; it appears, from experiment, that the comment in the PaperUnits article, saying that values are always in millimeters, prevails.

	;;;  (setq paperUnit 
	;;;  	(cond
	;;;  		((= acInches (vla-get-PaperUnits (vla-get-Layout (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object))))))
	;;;  			inch
	;;;  		)
	;;;  		((= acMillimeters (vla-get-PaperUnits (vla-get-Layout (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object))))))
	;;;  			millimeter
	;;;  		)
	;;;  		((= acPixels (vla-get-PaperUnits (vla-get-Layout (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object))))))
	;;;  			pixel
	;;;  		)
	;;;  		(T
	;;;  			nil
	;;;  		)
	;;;  	)
	;;;  )
	(setq sheetWidth  (* sheetWidth 	millimeter))
	(setq sheetHeight (* sheetHeight 	millimeter))


	(princ "paperUnits: ")(princ (vla-get-PaperUnits (vla-get-Layout (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object))))))(princ "\n")
	(princ "paperUnit: ")(princ paperUnit)(princ "\n")
	(princ "sheetWidth: ")(princ sheetWidth)(princ "\n")
	(princ "sheetHeight: ")(princ sheetHeight)(princ "\n")

	(setq layerStateManager (vla-getinterfaceobject (vlax-get-acad-object) "AutoCAD.AcadLayerStateManager.22"))
	
	(vlax-invoke-method layerStateManager "SetDatabase" (vla-get-Database (vla-get-Document mainPViewport)))
	

	
)








;;======== DEFINE THE DESIRED SETTINGS ========================================

(setq reportPosition 
	(list 
		(- sheetWidth  1.1  (nth 0 reportReferenceExtent))
		(- sheetHeight 1.1) 
		0
	)
)


;; the viewport box (my own concept) is the conceptual bounding box that is used for page layout and alignment of the viewport on the page
;; sometimes, we want to allow the model view to bleed beyond the viewport box, and to accomplish this, we manipulate the viewports true width, height, and center.
(setq viewportBoxWidth 24.5)
(setq viewportBoxHeight 15.9)
(setq viewportBoxTopLeft (list 1.5 (- sheetHeight 1.5)))
(setq viewportBoxCenter 	
	(list
		(+ (nth 0 viewportBoxTopLeft) (/ viewportBoxWidth 2)) ;;x coordinate
		(+ (nth 1 viewportBoxTopLeft) (* -1.0 (/ viewportBoxHeight 2)));; y coordinate
	)
)
(setq viewportBoxLowerLeft
	(mapcar '+
		viewportBoxCenter
		(mapcar	'(lambda (x) (/ x 2))
			(list (- viewportBoxWidth) (- viewportBoxHeight))
		)
	)
)

(setq viewportTopLeft viewportBoxTopLeft)


(setq viewportCameraTargetProjectedOntoPaper viewportBoxCenter)
;;desired properties:
;;defaults, to be overridden in document-specific sections below
(setq cameraTarget (list 0.0 0.0 0.0))
(setq cameraDirection (list 0 0  1.0))
(setq viewportTwistAngle 0)
(setq viewportScale (/ (/ 3.0 64.0) 12.0))
(setq viewportShadePlot 10)

(if (vl-string-search (strcase "Building 1") (strcase (vla-get-Name (vla-get-ActiveDocument (vlax-get-acad-object)))))
	(progn 
		(setq cameraTarget (list 2550.0 -1560 0.0))
	)
)

(if (vl-string-search (strcase "Building 2") (strcase (vla-get-Name (vla-get-ActiveDocument (vlax-get-acad-object)))))
	(progn 
		(setq cameraTarget (list 1718.381 -521.91 0.0))
		(setq cameraTarget
			(mapcar '+ cameraTarget
				(list
					(* (/ 1 viewportScale) -0.4) 
					(* (/ 1 viewportScale) -0.6)
					0
				)
			)
		)
		(setq viewportTwistAngle (* +90 degree))
	)
)

(if (vl-string-search (strcase "Building 3") (strcase (vla-get-Name (vla-get-ActiveDocument (vlax-get-acad-object)))))
	(progn 
		(setq cameraTarget (list 2580.0 -1218.00 0.0))
	)
)








;;I am expanding the viewport symmetrically in both directions.  This is a hack until I implement a full arbitrary model to paper transform.
(setq viewportWidth (+ 1 viewportBoxWidth))
(setq viewportHeight (+ 1 viewportBoxHeight))
(setq viewportCenter viewportBoxCenter)

;;after having computed the center using the nominal viewport width and height, we supersize the viewport to prevent clipping
; (setq viewportWidth (* 2 sheetWidth))
; (setq viewportHeight (* 2 sheetHeight))

;;(setq viewportVisualStyle 2)

;;acShadePlotAsDisplayed: Model space view plots the same way it is displayed.
;;acShadePlotHidden: Model space view plots with hidden lines removed, regardless of display.
;;acShadePlotRendered: Model space view plots as rendered regardless of display.
;;acShadePlotWireframe: : Model space view plots as wireframe regardless of display. 
;; 10: corresponds to the "Shaded" option in the UI dropdown box.  Not listed as an enum value in the manual.

(setq pathOfLayerStateFile 
	(strcat	
		( vl-filename-directory (vla-get-FullName (vla-get-Document mainPViewport))) 
		"\\..\\printerConfig\\printableLayerState.las"
	)
)
(setq nameOfLayerState "printableLayerState")
(princ "pathOfLayerStateFile: ")(princ pathOfLayerStateFile)(princ "\n")
(princ "nameOfLayerState: ")(princ nameOfLayerState)(princ "\n")

;;======== APPLY THE SETTINGS ========================================
(progn
	(vla-put-Layer mainPViewport "pViewports")
	
	
	(if (not (vl-string-search (strcase "cover sheet") (strcase (vla-get-Name (vla-get-ActiveDocument (vlax-get-acad-object))))))
		(progn 
			(vla-put-Width mainPViewport viewportWidth)
			(vla-put-Height mainPViewport viewportHeight)
			(vla-put-Center mainPViewport (vlax-3d-point viewportCenter))
			(vla-put-Target mainPViewport (vlax-3d-point cameraTarget))
			(vla-put-TwistAngle mainPViewport viewportTwistAngle)
			(vla-put-Direction mainPViewport (vlax-3d-point cameraDirection))
			(vla-put-CustomScale mainPViewport viewportScale)
			(vla-put-UCSPerViewport mainPViewport :vlax-false)
			(vla-put-UCSIconOn mainPViewport :vlax-false)
			;;(vla-put-VisualStyle mainPViewport viewportVisualStyle)
			(vla-put-ShadePlot mainPViewport viewportShadePlot)
			;(vla-put-DisplayLocked mainPViewport :vlax-false)

			;;the following viewCenter are the coordinates on the photograph paper (as if from the aforementioned camera) where we want our view to be centered.
			;; the coordinates on the photograph paper are set up so that the caeraTarget is projected onto <0, 0>
			(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewCenter/X" 0)
			(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewCenter/Y" 0)
			
			(if labelBlockReference
				(progn
					;;(vlax-dump-object labelBlockReference)
					(vla-put-InsertionPoint labelBlockReference 
						(vlax-3d-point 
							;;(getViewportLowerLeft mainPViewport)
							viewportBoxLowerLeft
						)
					)
				)
			)
		)
	)




	(if reportReference
		(progn
			(vla-put-InsertionPoint reportReference (vlax-3d-point reportPosition))
			;;(vla-put-InsertionPoint reportReference (vlax-3d-point (list 5 5 0)))
		)
	)

	
	
	;; we expect that the document has been manually prepared so that all layers are thawed in all viewports (which can be accomplished
	;; in the layer manager by selecting all layers, right-clicking, and in the context menu, click "VP Thaw Layer in All Viewports"
	;; There seems to be no programmatic way to thaw all layers in all viewports, or to set the pViewport-spoecific freeze status of a layer for a given pViewport, as far as I can tell.
	;; Therefore, we will not try to use pViewport-specific layer properties. (This would be a limitation in many applications, but fortunately not in the one at hand.).

	;;delete any exisiting layerState with the name nameOfLayerState, so that the subsequent importing of the layer state file will succeed.
	; (vl-catch-all-apply 'vla-Delete (list layerStateManager nameOfLayerState))
	; (vla-Import layerStateManager pathOfLayerStateFile)
	; (princ "(vlax-property-available-p layerStateManager \"Mask\"): ")(princ (vlax-property-available-p layerStateManager "Mask"))(princ "\n")
	; (princ "(vlax-get-property layerStateManager \"Mask\"): ")(princ (vlax-get-property layerStateManager 'Mask nameOfLayerState))(princ "\n")
	; (princ "(vla-get-Mask layerStateManager): ")(vla-get-Mask layerStateManager nameOfLayerState)(princ "\n")
	;;(vla-put-Mask layerStateManager acLsAll) 
	; (princ (strcat "(vlax-get-property layerStateManager \"Mask\" \"" nameOfLayerState "\"): "))(princ (vlax-get-property layerStateManager "Mask" nameOfLayerState))(princ "\n")
	; (vlax-put-property layerStateManager "Mask" nameOfLayerState acLsAll) ;;this specifies that we will restore all layer properties, rather than a subset of the properties.
	; (princ (strcat "(vlax-get-property layerStateManager \"Mask\" \"" nameOfLayerState "\"): "))(princ (vlax-get-property layerStateManager "Mask" nameOfLayerState))(princ "\n")
	; (princ "checkpoint3\n")
	;; it appears that the Mask property is an indexable property, and the indices are the names of the layerStates.  This makes sense, I just wish the manual would bother to mention it.
	
	;;delete any exisiting layerState with the name nameOfLayerState, so that the subsequent importing of the layer state file will succeed.
	(vl-catch-all-apply 'vla-Delete (list layerStateManager nameOfLayerState))
	(vla-Import layerStateManager pathOfLayerStateFile)
	(vla-Restore layerStateManager nameOfLayerState)
	(vlax-release-object layerStateManager)
	(command-s "_ZOOM" "ALL" "")
	;;observations re. layer state manager:
	;; when restoring a saved layer state (either programattically, as above, or using the 
	;; layer states manager ui, the layer manager dialog does not update its display to reflect the new layer status.  
	;; closing and reopening the layer manager dialog seems to fix this.
	;; I have observed that when restoring a saved layer state, either programatically or via the layer state manager ui, even when the mask has all properties selected,
	;; the layer state manager dialog still reports current layer state is "*UNSAVED".  I suspect this si related to the presence of layer names in the saved layer state that are not present in the current drawing.
	(princ)
)

(if titleBlockReference
	(progn
		(setAttribute titleBlockReference  "DATE"           "%<\\AcSm SheetSet.date>%\\pxsm0.7"                                        )
		(setAttribute titleBlockReference  "SHEET_NUMBER"   "%<\\AcSm Sheet.Number>% OF %<\\AcSm SheetSet.numberOfSheets>%\\pxsm0.7"   )
		(setAttribute titleBlockReference  "SCALE"          ""                                                                         )
		(setAttribute titleBlockReference  "SHEET_SIZE"     "ANSI D\\pxsm0.7"                                                          )
		(setAttribute titleBlockReference  "TITLE"          "%<\\AcSm Sheet.Title>%\\pxsm0.7"                                          )
		(setAttribute titleBlockReference  "CLIENT" 
			(strcat
				"{\\H0.85x;"
					"%<\\AcSm SheetSet.client_line1>%" "\n"
					"%<\\AcSm SheetSet.client_line2>%" "\n"
					"%<\\AcSm SheetSet.client_line3>%" "\n"
					"%<\\AcSm SheetSet.client_line4>%" "\n"
				"}"
			)
		)
		(setAttribute titleBlockReference "PROJECT" "%<\\AcSm.16.2 SheetSet.ProjectName>%\\pxsm0.7")
		
	)
)


(defun importMLeaderStyles ( /
		destinationDocument
		standardsDocument
		destinationMleaderStylesDictionary
		sourceMleaderStylesDictionary
		mleaderStyleToImport
		existingMleaderStyle
		mleadersReferringToTheExistingMLeaderStyle
		entity
		blockDefinition
		styleName
		mleaderReferringToTheExistingMLeaderStyle
		document
		result
	)

	(setq destinationDocument (vla-get-ActiveDocument (vlax-get-acad-object) ))
	(setq standardsDocument (LM:GetDocumentObject "standards.dws"))
	;; import mleaderstyles from the standards file.
	(if standardsDocument
		(progn
			(setq destinationMleaderStylesDictionary  (vla-item (vla-get-Dictionaries  destinationDocument  ) "ACAD_MLEADERSTYLE"))
			(setq sourceMleaderStylesDictionary       (vla-item (vla-get-Dictionaries  standardsDocument    ) "ACAD_MLEADERSTYLE"))

			(vlax-for mleaderStyleToImport sourceMleaderStylesDictionary
				(princ "now importing mleaderstyle ")(princ "\"")(princ (vla-get-Name mleaderStyleToImport))(princ "\"")(princ " from ")(princ "\"")(princ (vla-get-Name (vla-get-Document mleaderStyleToImport)))(princ "\"") (princ ".")(princ "\n")
				
				;; check for an existing MleaderStyle of the same name as mleaderStyleToImport.
				(setq existingMleaderStyle 
					(vl-catch-all-apply 'vla-item (list destinationMleaderStylesDictionary (vla-get-Name mleaderStyleToImport)))
				)
				; (if (vl-catch-all-error-p existingMleaderStyle)
					; (progn
						; (princ "\t")(princ "no mleaderstyle by that name already exists in the current document, as indicated by the error: ")(princ (vl-catch-all-error-message existingMleaderStyle))(princ "\n")
					; )
				; )
				(setq existingMleaderStyle
					(if (vl-catch-all-error-p existingMleaderStyle) nil existingMleaderStyle)
				)

				
				(setq mleadersReferringToTheExistingMLeaderStyle nil)
				(if existingMleaderStyle
					(progn
						; in this case, the active document already contains an Mleaderstyle having the name of the mleaderstyle we want to import.
						
						(vlax-for blockDefinition (vla-get-Blocks (vla-get-Document existingMleaderStyle))
							(vlax-for entity blockDefinition
								(if (= (vla-get-ObjectName entity) "AcDbMLeader")
									(progn
										(if (not (vl-catch-all-error-p (setq styleName (vl-catch-all-apply 'vla-get-StyleName (list entity)))))
											(progn
												(if (= styleName (vla-get-Name existingMleaderStyle))
													(progn
														(appendTo 'mleadersReferringToTheExistingMLeaderStyle entity)
													)
												)
											)
										)
									)
								)
							)					
						)
						
						(princ "\t")(princ "an mleaderStyle named ")(princ "\"")(princ (vla-get-Name mleaderStyleToImport))(princ "\"")(princ " already exists in this document, (being referenced by ")(princ (length mleadersReferringToTheExistingMLeaderStyle))(princ " mLeaders), so we will use Dictionary::Replace() to import the mleaderstyle.")(princ "\n")
						
						
						
						; (vla-Replace destinationMleaderStylesDictionary
							; (vla-get-Name existingMleaderStyle)
							; mleaderStyleToImport
						; )
						;;The Dictionary::Replace() function has the nasty side-effect of setting to null the "StyleName" property of any MLeaders that already reference the mleaderStyle.
						;; Dictionary::Replace() also mucks up the 'TextStyle' property of existing multileaders that reference the exisiting mleaderStyle.
						(vla-put-Name existingMleaderStyle (sanitizeName (GUID)))

						
					)
					(progn
						; in this case, the active document does not already contain an Mleaderstyle having the name of the mleaderstyle we want to import,
						; so we are free to simply copyObjects.
						(princ "\t")(princ "The current document does not yet contain an mleaderStyle named ")(princ "\"")(princ (vla-get-Name mleaderStyleToImport))(princ "\"")(princ ".")(princ "\n")

					)
				)
				
				(vla-CopyObjects 
					(vla-get-Document mleaderStyleToImport)  ; (should be equivalent to standardsDocument) ;	  ; the database whose "CopyObjects" method we are calling (this is the database from which we are copying things)
					(gc:ObjectListToVariant (list mleaderStyleToImport))		                                  ; the list of objects to be copied
					destinationMleaderStylesDictionary                                                            ; the owner to whom thses objects will be copied
				)
				
				;;restore the StyleName property of all the mleaders that refer to the existing mleader style.
				(foreach mleaderReferringToTheExistingMLeaderStyle mleadersReferringToTheExistingMLeaderStyle
					(vla-put-StyleName mleaderReferringToTheExistingMLeaderStyle (vla-get-Name mleaderStyleToImport) )
					(vl-catch-all-apply 'vlax-release-object (list mleaderReferringToTheExistingMLeaderStyle))
				)
				(vla-Delete existingMleaderStyle)
				(vlax-release-object mleaderStyleToImport)
			)
			(vl-catch-all-apply 'vlax-release-object (list mleaderStyleToImport))
			(Vlax-release-object destinationMleaderStylesDictionary)
			(Vlax-release-object sourceMleaderStylesDictionary)
		)
		(progn 
			(princ "no standards file could be found from which to import.\n")
			;do nothing
		)
	)
	
	
	(vlax-for document (vla-get-Documents (vlax-get-acad-object))
		(princ (vla-get-FullName document))(princ "\n")
	)
	(setq result (vlax-release-object standardsDocument))
	(princ "result of releasing: ")(princ result)(princ "\n")
	(gc) ;; I have found that this call to (gc) (garbage collection) is necessary in order to make Autocad reliably close the standards document.
	
	
	(princ)
)
(importMLeaderStyles)

;; the following qsave and close is necessary to make this script behave properly when called from executeScriptOnDrawing
(command-s "qsave")
(command-s "close")

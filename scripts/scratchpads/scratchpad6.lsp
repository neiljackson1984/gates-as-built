(setq mainPViewport (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))))
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewCenter/X" 0)
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewCenter/Y" 0)
;;(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewHeight" 10) ;;if you try to set ViewHeight to zero, a "ADS Request Error" message appears, and the ViewHight is not set
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewTarget/X" 0)
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewTarget/Y" 0)
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewTarget/Z" 0)
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewDirection/X" 0)
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewDirection/Y" 0)
(setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewDirection/Z" 1)

(vla-put-Width mainPViewport 3)
(vla-put-Height mainPViewport 6)
(reportOnPViewport (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))))
(princ)




		(setq assumedSheetWidth 34)
		(setq assumedSheetHeight 22)
		(setq sheetWidth assumedSheetWidth)
		(setq sheetHeight assumedSheetHeight)
		;;desired properties:
		(setq cameraTarget (list 2400.0 -1063.0 0.0))
		(setq cameraDirection '(0 0  1.0))
		(setq viewportWidth 23.0)
		(setq viewportHeight 15.0)
		(setq viewportTopLeft (list 2.0 (- sheetHeight 2.0)))
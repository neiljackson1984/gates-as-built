(setq weAreInAnAcadDocScript T) ;;this should be the first line of this acaddoc script
(defun LM:sfsp+ ( lst )
    (   (lambda ( str lst )
            (if (setq lst
                    (vl-remove-if
                       '(lambda ( x )
                            (or (vl-string-search (strcase x) (strcase str))
                                (not (findfile x))
                            )
                        )
                        lst
                    )
                )
                (setenv "ACAD" (strcat str ";" (apply 'strcat (mapcar '(lambda ( x ) (strcat x ";")) lst))))
            )
        )
        (vl-string-right-trim ";" (getenv "ACAD"))
        (mapcar '(lambda ( x ) (vl-string-right-trim "\\" (vl-string-translate "/" "\\" x))) lst)
    )
)


 
(defun getDirectoryOfActiveDocument ()
	(vl-filename-directory (vla-get-FullName (vla-get-ActiveDocument (vlax-get-acad-object))))
)


;; if you want to load another acaddoc.lsp script (in the parent directory of the current script, for instance),
;; use the function (loadAScript ), which will set the directoryOfThisScript variable and then load
;; the requested script.  This function pays attention to the variable weAreInAnAcadDocScript, which the user should
;; should set at the beginning of each acaddoc.lsp file and unset at the end.  (the LoadAScript() function also unsets weAreInAnAcadDocScript
;; immediately before loading the new script, and then sets it immediately afterward.
;; As long as all loads within this acaddoc.lsp file are done via the loadAScript() function, the getDirectoryOfThisScript() function should work as expected (it might return nil, but if it returns a string, then you can be confident that that string is the directory of the current script).
(defun getDirectoryOfThisScript ()
	(if directoryOfThisScript 
		directoryOfThisScript 
		(if weAreInAnAcadDocScript 
			(vl-filename-directory (findfile "acaddoc.lsp"))
			nil
		)
	)
)

(defun loadAScript (x /
	initialValueOfWeAreInAnAcadDocScript
	initialValueOfDirectoryOfThisScript
	)
	(if (not (findfile x)) (alert (strcat "warning from " (getDirectoryOfThisScript) "\\" "acaddoc.lsp: \n" "failed to find file " x)))
	(setq initialValueOfDirectoryOfThisScript directoryOfThisScript)
	(setq directoryOfThisScript (vl-filename-directory (findfile x)))
	(setq initialValueOfWeAreInAnAcadDocScript weAreInAnAcadDocScript)
	(setq weAreInAnAcadDocScript nil)
	(load x)
	(setq weAreInAnAcadDocScript initialValueOfWeAreInAnAcadDocScript)
	(setq directoryOfThisScript initialValueOfDirectoryOfThisScript)
	(princ)
)
;;===========================================
;; DO NOT MODIFY ABOVE THIS LINE 
;; THE MEAT OF THIS SCRIPT GOES BELOW
;;===========================================

;;(alert (strcat "ahoy from " (getDirectoryOfThisScript)))
(loadAScript (strcat (getDirectoryOfThisScript) "\\" "..\\..\\acaddoc.lsp"))


;;===================================================
;;  DO NOT MODIFY BELOW THIS LINE
;;===================================================
(setq weAreInAnAcadDocScript nil)(princ) ;;this should be the last line of this script.

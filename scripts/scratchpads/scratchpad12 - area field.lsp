;(setq ename (car (nentsel )))
(setq ename (car (entsel )))

(setq entity (vlax-ename->vla-object ename))
(princ "\n")
(princ (vla-get-ObjectName entity)) (princ " ")
(princ (vla-get-Handle entity))(princ " ")
(princ 
	(vl-catch-all-apply 
		'vla-get-Name 
		(list 
			(vl-catch-all-apply 
				'vla-get-Document 
				(list 
					entity
				)
			)
		)
	)
)
(princ "\n")
(princ)

(setq owner 
	(vl-catch-all-apply 
		'vla-ObjectIdToObject
		(list 
			(vla-get-ActiveDocument (vlax-get-acad-object) )
			(vla-get-OwnerID entity)
		)
	)
)

(princ "owner: ")(princ (vla-get-ObjectName owner))(princ "\n")
(princ "owner.IsXRef: ")(princ (= (vla-get-IsXRef owner) :vlax-true))(princ "\n")
(vlax-dump-object owner)

(vlax-dump-object entity)



; (vlax-dump-object (vla-HandleToObject (vla-get-ActiveDocument (vlax-get-acad-object) ) (vla-get-Handle entity)))
; (princ (vla-get-ObjectName (vlax-ename->vla-object (car (entsel )))))(princ "\n")

; (vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object) ))
	; (princ  (vla-get-ObjectName blockDefinition))(princ "\n")
; )

; (setq mySelectionSet (ssadd))
; (foreach explosionProduct explosionProducts
	; (setq mySelectionSet 
		; (ssadd  
			; (handent (vla-get-Handle explosionProduct))
			; mySelectionSet
		; )
	; )
; )
(princ "\n")
(princ ename)(princ "\n")

(setq tempGroupName "ahoy2")
(setq tempGroup 
	(vl-catch-all-apply 'vla-Item 
		(list 
			(vla-get-Groups  (vla-get-ActiveDocument (vlax-get-acad-object) )) 
			tempGroupName
		)
	)
)
(if (vl-catch-all-error-p tempGroup)
	(progn
		(setq tempGroup (vla-Add (vla-get-Groups  (vla-get-ActiveDocument (vlax-get-acad-object) )) tempGroupName))
	)
)

;;make suere the group is empty

(while (> (vla-get-Count tempGroup) 0)
	(vla-RemoveItems tempGroup
		(gc:ObjectListToVariant 
			(list (vla-Item tempGroup 0))
		)
	)
)
(princ "tempGroup.count: ")(princ (vla-get-Count tempGroup))(princ "\n")


(vla-AppendItems tempGroup (gc:ObjectListToVariant (list entity)))

(princ "tempGroup.count: ")(princ (vla-get-Count tempGroup))(princ "\n")



(vlax-dump-object tempGroup)

(dumpallproperties  (vlax-vla-object->ename tempGroup))

(princ "\n")
; (princ "ActiveSelectionSet.Count: " )(princ (vla-get-Count (vla-get-ActiveSelectionSet (vla-get-ActiveDocument (vlax-get-acad-object) ))))(princ "\n")
; (vla-AddItems (vla-get-ActiveSelectionSet (vla-get-ActiveDocument (vlax-get-acad-object) ))
	; (gc:ObjectListToVariant (list entity))
; )

; (princ "ActiveSelectionSet.Count: " )(princ (vla-get-Count (vla-get-ActiveSelectionSet (vla-get-ActiveDocument (vlax-get-acad-object) ))))(princ "\n")

;%<\AcObjProp.16.2 Object(%<\_ObjId 2342882758496>%).Area \f "%lu2%pr0%ps[, SQ. FT.]">%

;(command "SELECT" ename)

; %<\AcObjProp Object(%<\_ObjId 2341055333776>%).Area \f "%lu2">%
; 000002217e4c75f8
; The ObjId value that appears in the field code is the ename number, expressed in decimal.


; %<\AcObjProp Object(%<\_ObjId ‭2342883874960‬>%).Area \f "%lu2">%
(princ "entity.ObjectId: ")(princ (vla-get-ObjectId entity))(princ "\n")
(princ "entity: ")(princ entity)(princ "\n")
(princ "ename: ")(princ ename)(princ "\n")
; (princ "int(ename): ")(princ (+ 0  ename))(princ "\n")
(princ "objectID: ")(princ (getpropertyvalue ename "ObjectId"))(princ "\n")
; (command "SELECT " ename)
(princ "utility.GetObjectIdString(entity, false): ")(princ (vla-GetObjectIdString (vla-get-Utility  (vla-get-ActiveDocument (vlax-get-acad-object) )) entity :vlax-false) )(princ "\n")
(setq objectIdString (vla-GetObjectIdString (vla-get-Utility  (vla-get-ActiveDocument (vlax-get-acad-object) )) entity :vlax-false))
(setq areaFieldCode 
	(strcat 
		"%<\\AcObjProp.16.2 Object(%<\\_ObjId " 
		objectIdString 
		">%).Area \\f \"%lu2%pr1%ps[, square feet]%ct8[0.0069444444444444]\">%"
	)
)
(princ "areaFieldCode: ")(princ areaFieldCode)(princ "\n")

; (sssetfirst nil (ssadd ename))




(princ)


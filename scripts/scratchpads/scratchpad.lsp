
(setq numRows 4)
(setq numColumns 3)
(setq reportTable 
	(vla-AddTable (vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
		(vlax-3D-point (list 0 0 0)) ;;insertion point
		numRows ;;numRows
		numColumns ;;numColumns
		0.01 ;; rowHeight
		2 ;; column width
	)
)
(foreach rowIndex (range (- numRows 1) )
	(foreach columnIndex (range (- numColumns 1))
		(setq gridVisibilityReportString 
			(apply 'strcat
				(mapcar 
					'(lambda (x) 
						if(
							(= 
								:vlax-true 
								(vla-GetCellGridVisibility reportTable 
									rowIndex
									columnIndex
									x
								)
							)
							"1"
							"0"
						)
					)
					(list acLeftMask acTopMask acRightMaskacBottomMask)
				)
			)
		)
		(vla-setText reportTable 
			rowIndex
			columnIndex
			(strcat 
				(strcat "ahoy " (itoa rowIndex) "-" (itoa columnIndex) "\n")
			)
		)
		
	)
)




(vlax-dump-object (vlax-ename->vla-object (car (entsel))))










(vla-put-Layer			
	(vlax-ename->vla-object 
		(handent
			;;(getstring T "Enter the handle of the main PViewport: ")
			;;"D290B" ;;hack for the particular template I am using
			"D83CF"
		)
	)
	"Defpoints"
)

			(vla-Delete (vlax-ename->vla-object 
				(handent
					;;(getstring T "Enter the handle of the main PViewport: ")
					"D290B" ;;hack for the particular template I am using
				)
			))


(vla-put-VisualStyle (vlax-ename->vla-object (car (entsel))) 2)
(vlax-put-property (vlax-ename->vla-object (car (entsel))) 'HiddenLinesRemoved :vlax-true)


(princ "owner: ")(princ (vla-get-Name (vla-ObjectIDToObject (vla-get-ActiveDocument (vlax-get-acad-object)) (vla-get-OwnerID (vlax-ename->vla-object (car (nentsel)))))))(princ "\n")
(princ "owner: ")(princ (vla-get-EffectiveName (vla-ObjectIDToObject (vla-get-ActiveDocument (vlax-get-acad-object)) (vla-get-OwnerID (vlax-ename->vla-object (car (nentsel)))))))(princ "\n")

(vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))bedit
	(princ (vla-get-Name blockDefinition))(princ "\n")(princ)
)

(vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	(vlax-for entity blockDefinition
		(if 
			(= "AcDbBlockReference" (vla-get-ObjectName entity))
			(progn
				(princ "found a reference to ")(princ (vla-get-Name entity))(princ " within ")(princ (vla-get-Name blockDefinition))(princ " .")(princ "\n")(princ)
			)
		)
		(if 
			(= "AcDbAttribute" (vla-get-ObjectName entity))
			(progn
				(princ "found an attribute ")(princ (vla-get-TagString entity))(princ " within ")(princ (vla-get-Name blockDefinition))(princ " .")(princ "\n")(princ)
			)
		)
	)
)

(vlax-dump-object (vla-Item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*U155") )
(vla-get-EffectiveName (vla-Item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*U186") )
(vla-Delete (vla-Item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*U155") )
(vla-Delete (vla-Item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*U167") )

(vlax-for entity (vla-Item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*U155")
	(princ (vla-get-ObjectName entity))(princ "\n")(princ)
)

(vlax-for entity (vla-Item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*U155")
	(princ (vla-get-ObjectName entity))(princ "\n")(princ)
)


(progn
	
	; (setq sheetNumber	"A1.0"		    )
	; (setq sheetName     "LEVEL 0"		)
	
	; (setq sheetNumber	"A2.0"		    )
	; (setq sheetName     "LEVEL 1"		)
	
	(setq sheetNumber	"A3.0"		    )
	(setq sheetName     "LEVEL 2"		)
	
	(setq date          "2017/01/09"	)	
	(setq projectTitle "")
				
	(setq siteAddress
		(strcat
			"7721 19TH AVENUE NORTHEAST" "\n"
			"SEATTLE, WA"
		)
	)
	(setq drawnBy 	    "Neil Jackson"	)
	(setq revision      "1"				)
	(setq checkedBy		""			    )



	(setq titleBlockReference (vlax-ename->vla-object (car (entsel "select the titleBlock reference: "))))0
	(princ "\n")
	(setq titleTextBlockReference (vlax-ename->vla-object (car (entsel "select the titleText block reference: "))))
	(princ "\n")

	(setAttribute titleBlockReference "SHEETTITLE"
		; (strcat
			; "{"
				; "\\fArial|b0|i0|c0|p34;\\H1.25x;" projectTitle "\n\n"
				; "\\H0.6x;" siteAddress 
			; "}"
		; )		
		(strcat
			"{"
				"\\fArial|b0|i0|c0|p34;\\H1.25x;" siteAddress "\n\n"
			"}"
		)
	)
	(setAttribute titleBlockReference "DRAWNBY"			(strcat "\\W0.9000;" drawnBy)       )
	(setAttribute titleBlockReference "REVISION"		(strcat "\\W0.9000;" revision)      )
	(setAttribute titleBlockReference "CHECKEDBY"		(strcat "\\W0.9000;" checkedBy)     )
	(setAttribute titleBlockReference "SHEETNUMBER"		(strcat "\\W0.9000;" sheetNumber)   )
	(setAttribute titleBlockReference "SHEETNAME"		(strcat "\\W0.9000;" sheetName)     )
	(setAttribute titleBlockReference "DATE"			(strcat "\\W0.9000;" date)          )
	
	
	(setAttribute titleTextBlockReference "TITLE"		(strcat "\\W0.9000;" sheetName)     )
	(setAttribute titleTextBlockReference "XX"		    (strcat "\\W0.9000;" "1")           )
	
	
)
;======

;; note: the text field code for the scale marker is (with apporpriate objectid susbstituted): 
;;     \W0.7000;%<\AcObjProp Object(%<\_ObjId 140697600720496>%).CustomScale \f "%lu4%pr8%ct2%qf2816 = 1'-0\"">%



(quit)


(vla-get-ObjectId (vlax-ename->vla-object (car (entsel))))
(vla-get-Handle (vlax-ename->vla-object (car (entsel))))
(entget (car (entsel)))








(setvar "PARAMETERCOPYMODE" 0) ;; Do not copy any dimensional constraints or constraint parameters. if this is set to 1, it fucks up the explosion routine.
(setq myBlockReference (vlax-ename->vla-object (car (entsel))))
(princ "fully exploding reference to ")(princ (vla-get-EffectiveName myBlockReference))(princ "\n")
(fullyExplodeBlockReference myBlockReference)
(vla-Delete myBlockReference)
(princ "(vlax-erased-p myBlockReference): ")(princ (vlax-erased-p myBlockReference))(princ "\n")
(princ "PURGING\n")
(setvar "CLAYER" "0") ;; purge will not get rid of the layer that happens to be current, even if it is otherwise purgeable.
(sssetfirst nil ) (command)(command)
(command-s 
	"._-PURGE"
	"All" ;;types of objects to purge
	"*" ;;names to purge 
	"No" ;;verify each name to be purged?

)
(princ "PURGING AGAIN\n")		
(setvar "CLAYER" "0") ;; purge will not get rid of the layer that happens to be current, even if it is otherwise purgeable.
(sssetfirst nil ) (command)(command)
(command-s 
"._-PURGE"
"All" ;;types of objects to purge
"*" ;;names to purge 
"No" ;;verify each name to be purged?

)
(princ)








(load "acaddoc.lsp")
(reportOnBlockDependence)



(vlax-dump-object 
	(vla-ObjectIDToObject 
		(vla-get-ActiveDocument (vlax-get-acad-object)) 
		(vla-get-OwnerID 
			(vlax-ename->vla-object (car (nentsel)))
		)
	)
)

(vlax-dump-object (vla-get-ActiveDocument (vlax-get-acad-object)) )
(LM:dump (vla-get-ActiveDocument (vlax-get-acad-object)) )

(load "annotateDoorHinges5.lsp")
(setq myBlockReference (vlax-ename->vla-object (car (entsel))))
(princ "fully exploding reference to ")(princ vla-get-EffectiveName myBlockReference)(princ "\n")
(fullyExplodeBlockReference myBlockReference)
(vla-Delete myBlockReference)
(princ "(vlax-erased-p myBlockReference): ")(princ (vlax-erased-p myBlockReference))(princ "\n")
(princ)

(setq myBlockReference (vlax-ename->vla-object (car (entsel))))
(vla-Explode myBlockReference)


(load "regionFunctions.lsp")(playground1)

(LM:Unique (list 1 2 3 4 5 3 6 3 7 3 8 5 6 1 99))

(defun foo ( / ) "foo was called with no arguments")
(defun foo ( a / ) 1)
(defun foo ( a b / ) (list 23 45))
(princ (foo ))
(princ (foo "asdfasdf"))
(princ (foo "asdf" "asdfasd"))
(princ )
; the autocad 2016 manual mentions the possibility of overloading function names, but the same comment does not appear in the acad2013 manual, which suggest that no function overloading is possible in acad2013 autolisp.

(Setq myEname (car (nentsel )))
(setRank 
	myEname
	(list )
)
(princ (getRank myEname))(princ)


(mapcar 'numberp (read "(1 2 3)"))
(mapcar 'strlen (read "(1,2,3)"))
(mapcar 'stringp (read "(1,2,3)"))
(mapcar 'strlen (list "1,2,3"))
(strlen (list "1,2,3"))

(if (Setq entry (getstring T "enter something")) (princ (strcat "you entered: '" entry "'" "\n")) (princ "you enetered nil."))


(listp (read "(1,2,3)"))
(length (read "(1 2 3)"))
(listp (read "1 2 3"))
(load "polygonRanking.lsp")
(Setq myEname (car (nentsel )))
(setRank 
	myEname
	(list 111 222 333 444 555)
)
(getRank myEname)

(load "polygonRanking.lsp")
(Setq myEname (car (nentsel )))
(setRank 
	myEname
	999
)
(getRank myEname)


(assoc -3 (entget (car (nentsel )) (list "polygonRanking" "neil")))
(assoc -3 (entget (car (nentsel )) (list )))

(mapcar 'cdr 
	; (vl-remove-if-not '(lambda (x) (= xDataGroupCodeForInteger (car x))) 
	(vl-remove-if-not '(lambda (x) (= 1070 (car x))) 
		(cdr
			(cadr
				(assoc -3 (entget (car (nentsel )) (list "polygonRanking")))
			)
		)
	)
)



(assoc 1 
	(list
		(cons 1  11 )
		(cons 2  22 )
		(cons 3  33 )
		(cons 1  101 )
	)
)

(setq myList
	(list
		(cons 1  11 )
		(cons 2  22 )
		(cons 3  33 )
		(cons 1  101 )
	)
)
(mapcar 'cdr (vl-remove-if-not '(lambda (x) (= 1 (car x))) myList))
(mapcar 'cdr (list ))


(setq myList
	(list
		(cons 1  11 )
		(cons 2  22 )
		(cons 3  33 )
		(cons 1  101 )
	)
)

(cdr (assoc 121 myList))

(load "acaddoc.lsp")
(fixRoomPolygonRanks)

(loadData)
(populateDoors)
(drawDoorIcon (cdr (nth 2 doors)))


(loadData)
(populateDoors)
(drawDoorIcon (cdr (nth 0 doors)))

(setq acadObj (vlax-get-acad-object))
(setq doc (vla-get-ActiveDocument acadObj))
(setq modelSpace (vla-get-ModelSpace doc))
(setq blocks (vla-get-Blocks doc))
(setq entitiesToDelete (list))

(vlax-for blockDefinition blocks
	(vlax-for entity blockDefinition
		(progn
			(if 
				(and
					(= "AcDbBlockReference" (vla-get-objectname entity))
					(not 
						(= "0" (vla-get-Layer entity))
					)
				)
				(progn						
					(princ "moved a block reference pointing to ")(princ (vla-get-Name entity))(princ " from layer ")(princ (vla-get-Layer entity))(princ " to layer 0.")(princ "\n")
					(vla-put-Layer entity "0")
				)
			)
		)
	)
)
(princ)



CLIPROMPTUPDATE
(princ "cmdactive: ")(princ (getvar 'cmdactive))(princ "\n")(princ)
(getvar "CMDACTIVE")

(setq handlesOfConstructionEntities (list ))
(annotateAllDoorAndWindowLines)
(princ handlesOfConstructionEntities)
(princ "\n")
(princ "ahoy")
(princ)

(exit)

(vlax-dump-object (vlax-ename->vla-object (handent "C914A")))

(setq handlesOfConstructionEntities (list ))

(isBConstruction (handent "C94FB"))
(isBConstruction (handent "C9626"))

(vlax-dump-object (vlax-ename->vla-object (handent "C9643")))
(vlax-dump-object (vlax-ename->vla-object (car (entsel))))

(cdr (assoc -3 (entget (car (entsel)) (list "ACAD_BCONSTRUCTION"))))

(isBConstruction (car (entsel)))
(isBConstruction (car (nentsel)))

(load "dictionaryFunctions.lsp")
(keys 
	(vla-GetExtensionDictionary 
		(vlax-ename->vla-object (car (entsel)))
	)
)


	(vla-Get-HasExtensionDictionary 
		(vlax-ename->vla-object (car (entsel)))
	)
	
(vlax-dump-object
		(vlax-ename->vla-object (car (entsel)))
)

(vlax-dump-object
	(vla-GetExtensionDictionary 
		(vlax-ename->vla-object (car (entsel)))
	)
)

(princ 	
	(keys
		(vla-GetExtensionDictionary 
			(vlax-ename->vla-object (car (entsel)))
		)
	)
)

(princ
	(vla-get-Count
		(vla-GetExtensionDictionary 
			(vlax-ename->vla-object (car (entsel)))
		)
	)
)


(setq bconstructionApplicationName "ACAD_BCONSTRUCTION")
(entget (car (entsel)) (list "ACAD_BCONSTRUCTION"))
(princ
	(entget (car (entsel)))
)


	(setq xDataGroupCodeForInteger 1070) 
				(cdr 
					(assoc xDataGroupCodeForInteger 
						(cdr 
							(nth 0 
								(cdr
									(assoc -3 (entget (car (entsel)) (list "ACAD_BCONSTRUCTION")))
								)
							)
						)
					)
				)
				
(isBConstruction (car (nentsel)))
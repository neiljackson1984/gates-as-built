(setq entity (vlax-ename->vla-object (car (nentsel ))))
(princ "\n")
(princ (vla-get-ObjectName entity)) (princ " ")
(princ (vla-get-Handle entity))(princ " ")
(princ 
	(vl-catch-all-apply 
		'vla-get-Name 
		(list 
			(vl-catch-all-apply 
				'vla-get-Document 
				(list 
					entity
				)
			)
		)
	)
)
(princ "\n")
(princ)

(setq owner 
	(vl-catch-all-apply 
		'vla-ObjectIdToObject
		(list 
			(vla-get-ActiveDocument (vlax-get-acad-object) )
			(vla-get-OwnerID entity)
		)
	)
)

(princ "owner: ")(princ (vla-get-ObjectName owner))(princ "\n")
(princ "owner.IsXRef: ")(princ (= (vla-get-IsXRef owner) :vlax-true))(princ "\n")
(vlax-dump-object owner)

(vlax-dump-object entity)

; (vlax-dump-object (vla-HandleToObject (vla-get-ActiveDocument (vlax-get-acad-object) ) (vla-get-Handle entity)))
; (princ (vla-get-ObjectName (vlax-ename->vla-object (car (entsel )))))(princ "\n")

; (vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object) ))
	; (princ  (vla-get-ObjectName blockDefinition))(princ "\n")
; )
; ;;(vlax-dump-object (vlax-ename->vla-object (car (entsel))))
;;  (dumpallproperties (car (entsel)))

(vla-put-PlotStyleName (vlax-ename->vla-object (car (entsel))) "myCoolPlotStyle")

(vlax-for entity (vla-item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "level1-building2")
	;;(vlax-dump-object entity)
	(princ (vla-get-Handle entity))(princ ": ")(princ (vla-get-ObjectName entity))(princ "\n")
)


(vlax-for entity (vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
	;;(vlax-dump-object entity)
	(princ (vla-get-Handle entity))(princ ": ")
	(princ (vla-get-ObjectName entity))(princ "\n")(princ)
)




2A1: AcDbLine
2A2: AcDbPolyline
2A3: AcDbAlignedDimension

(vlax-dump-object (vla-handleToObject (vla-get-ActiveDocument (vlax-get-acad-object)) "2A3"))
(vlax-dump-object (vla-handleToObject (vla-get-ActiveDocument (vlax-get-acad-object)) "C8768"))

(defun centerDimensionText (dimension / newtextpoint)
	(setq newtextpoint (mapcar '(lambda (x y) (/ (+ x y) 2))
				(gc:VariantToLispData (vla-get-ExtLine1Point dimension))
				(gc:VariantToLispData (vla-get-ExtLine2Point dimension))
			))
	(princ "setting text position to ")(princ newtextpoint)(princ "\n")
	(vla-put-TextPosition dimension
		(vlax-3d-point 
			newtextpoint
		)
	)
	(princ)
)


(setq myDimension (vla-handleToObject (vla-get-ActiveDocument (vlax-get-acad-object)) "C8768"))
(princ "before centering, text position is: ")(princ (gc:VariantToLispData (vla-get-TextPosition myDimension))) (princ "\n")
(centerDimensionText myDimension)
(princ "after  centering, text position is: ")(princ (gc:VariantToLispData (vla-get-TextPosition myDimension))) (princ "\n")
(princ)



(vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	(if 
		(and
			(/= (vla-get-handle blockDefinition) (vla-get-handle (vla-get-ModelSpace (vla-get-Document blockDefinition))))
			(/= (vla-get-handle blockDefinition) (vla-get-handle (vla-get-PaperSpace (vla-get-Document blockDefinition))))
		)
		(progn
			(vlax-dump-object blockDefinition)
		)
	)
)

(vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	(if 
		(= :vlax-true (vla-get-IsXRef blockDefinition))
		(progn
			(vlax-dump-object blockDefinition)
		)
	)
)

(vlax-for fileDependency (vla-get-FileDependencies (vla-get-ActiveDocument (vlax-get-acad-object)))
	(vlax-dump-object fileDependency)
)

(setq i 1)
(vlax-for dictionary (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object)))
	;;(if (= "AcDbDictionary" (vla-get-ObjectName dictionary))
	(setq theName (vl-catch-all-apply 'vla-get-Name (list dictionary)) )
	(if (vl-catch-all-error-p theName)
		(setq theName nil)
	)
	(princ "dictionary ")(princ i)(princ ": ")
	(if theName
		(progn
			(princ theName)
		)
		(progn
			(vlax-dump-object dictionary)
		)
	)
	(princ "\n")
	(plusPlus 'i)
)

(vlax-for item (vla-item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD_IMAGE_DICT")
	(vlax-dump-object item)
	(princ "\n")
	;;(vlax-dump-object (vla-ObjectIDToObject (vla-get-Document item) (vla-get-ObjectID item)))
	;;(princ "\n")
	(dumpallproperties (vlax-vla-object->ename item))
	;(vl-catch-all-apply 'vla-Delete (list item))
	(princ "has a SourceFileName property: ")(princ (vlax-property-available-p item "SourceFileName") )
)



; ;;(vla-Delete (vla-HandleToObject (vla-get-ActiveDocument (vlax-get-acad-object)) "B4BD"))

; (vlax-dump-object (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))))

; (vlax-dump-object  (vla-get-Layouts (vla-get-ActiveDocument (vlax-get-acad-object))))

; (vlax-dump-object  (vla-get-ActiveLayout (vla-get-ActiveDocument (vlax-get-acad-object))))
; (vlax-dump-object  (vla-get-ActivePViewport (vla-get-ActiveDocument (vlax-get-acad-object))))
; (vlax-dump-object  (vla-get-Block (vla-get-ActiveLayout (vla-get-ActiveDocument (vlax-get-acad-object)))))

; (vlax-dump-object  (vla-get-Block (vla-Item (vla-get-Layouts (vla-get-ActiveDocument (vlax-get-acad-object))) "building 1 - level 1")))


; (vlax-dump-object  (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object))))


; (vlax-dump-object 
	; (vla-get-ViewPorts (vla-get-ActiveDocument (vlax-get-acad-object)))
; )

; ;;(princ (keys (vla-get-ViewPorts (vla-get-ActiveDocument (vlax-get-acad-object)))))(princ)

; (vlax-dump-object 
	; (vla-Item	
		; (vla-get-ViewPorts (vla-get-ActiveDocument (vlax-get-acad-object)))
		; 0
	; )
; )




( ;; configure paperSpace and viewports
	(lambda 
		(
			/
			mainPViewport
			entity
			acadObj 
			doc 
			modelSpace 
			layers 
			groups
			blockDefinitions
			views
			cameraTarget
			sheetHeight
			sheetWidth
			cameraDirection
			thisName
			names
			currentViewCenter
			newViewCenter
			XDataType
			XDataValue
			layout
			paperSpace
			owner
			pViewports
			pViewport
			viewportVisualStyle
		) 
		

		
		(setq acadObj (vlax-get-acad-object))
		(setq doc (vla-get-ActiveDocument acadObj))
		(setq modelSpace (vla-get-ModelSpace doc))
		(setq layers (vla-get-Layers doc))
		(setq groups (vla-get-Groups doc))
		(setq blockDefinitions (vla-get-Blocks doc))
		(setq views (vla-get-Views doc))
		(setq paperSpace (vla-get-PaperSpace doc))
		
		
		
		(setq pViewports (list))
		(setq mainPViewport nil)
		(vlax-for entity paperSpace
			(if 
				(and
					(= "AcDbViewport" (vla-get-ObjectName entity))
				)
				(progn
					(setq pViewports (cons entity pViewports))
					;;(vlax-dump-object entity)
					(setq names (list ))
					(setq i 0)
					(while (< i (vla-get-Count (vla-GetExtensionDictionary entity)))  
						(setq names
							(append
								names
								(list 
									(vla-GetName
										(vla-GetExtensionDictionary entity)
										(vla-Item (vla-GetExtensionDictionary entity) i)
									)
								)
							)
						)
						(setq i (+ 1 i))
					)
					
					
					(princ "===========================\n")
					(princ "found an AcDbViewport object: ")(princ "\n")
					(princ "\t\t\t")(princ "owner: ")(princ (vla-get-Name (vla-ObjectIDToObject doc (vla-get-OwnerID entity))))(princ "\n")
					(princ "\t\t\t")(princ "handle: ")(princ (vla-get-Handle entity))(princ "\n")
					(princ "\t\t\t")(princ "ObjectId: ")(princ (vla-get-ObjectId entity))(princ "\n")
					(princ "\t\t\t")(princ "width: ")(princ (vla-get-Width entity))(princ "\n")
					(princ "\t\t\t")(princ "height: ")(princ (vla-get-Height entity))(princ "\n")
					(princ "\t\t\t")(princ "center: ")(princ (gc:VariantToLispData (vla-get-Center entity)))(princ "\n")
					(princ "\t\t\t")(princ "target: ")(princ (gc:VariantToLispData (vla-get-Target entity)))(princ "\n")
					(princ "\t\t\t")(princ "direction: ")(princ (gc:VariantToLispData (vla-get-Direction entity)))(princ "\n")
					(princ "\t\t\t")(princ "visualStyle: ")(princ (vla-get-VisualStyle entity))(princ "\n")
					(princ "\t\t\t")(princ "hasSheetView: ")(princ (vla-get-HasSheetView entity))(princ "\n")
					(princ "\t\t\t")(princ "modelView: ")(princ (vla-get-ModelView entity))(princ "\n")
					(princ "\t\t\t")(princ "sheetView: ")(princ (vla-get-SheetView entity))(princ "\n")
					(princ "\t\t\t")(princ "hasSheetView: ")(princ (vla-get-HasSheetView entity))(princ "\n")
					(princ "\t\t\t")(princ "HasExtensionDictionary: ")(princ (vla-get-HasExtensionDictionary entity))(princ "\n")
					(princ "\t\t\t")(princ "names in extension dictionary: ")(princ names)(princ "\n")
					(princ "\t\t\t")(princ "\"ACAD\" xdata: ")(princ (gc:GetXdata entity "ACAD"))(princ "\n")
					(princ "\t\t\t")(princ "ShadePlot: ")
						(princ
							(cdr 
								(assoc  (vla-get-ShadePlot entity)
									(list
										(cons acShadePlotAsDisplayed "acShadePlotAsDisplayed")
										(cons acShadePlotHidden "acShadePlotHidden")
										(cons acShadePlotRendered "acShadePlotRendered")
										(cons acShadePlotWireframe "acShadePlotWireframe")
									)
								)
							)
						)
					(princ "\n")
					
					
					(if (= :vlax-true (vla-get-HasSheetView entity))
						(progn
							(setq mainPViewport entity)
						)
					)
				)
			)			
		)
		
		;;(foreach pViewport pViewports(vlax-dump-object pViewport))
		
		(setq assumedSheetWidth 34)
		(setq assumedSheetHeight 22)
		(setq sheetWidth assumedSheetWidth)
		(setq sheetHeight assumedSheetHeight)
		;;desired properties:
		(setq cameraTarget (list 2400.0 -1063.0 0.0))
		(setq cameraDirection '(0 0  1.0))
		(setq viewportWidth 23.0)
		(setq viewportHeight 15.0)
		(setq viewportTopLeft (list 2.0 (- sheetHeight 2.0)))
		(setq viewportCenter 
			(list
				(+ (nth 0 viewportTopLeft) (/ viewportWidth 2)) ;;x coordinate
				(+ (nth 1 viewportTopLeft) (* -1.0 (/ viewportHeight 2)));; y coordinate
			)
		)
		;;(setq viewportVisualStyle 2)
		(setq viewportShadePlot acShadePlotHidden)
		;;HiddenLinesRemoved 
		(setq viewportScale (/ (/ 3.0 64.0) 12.0))
		
		

		
		
		
		;; from http://help.autodesk.com/view/ACD/2016/ENU/?guid=GUID-2602B0FB-02E4-4B9A-B03C-B1D904753D34 :
		; ; 0 = 2D Optimized (classic 2D)
		; ; 1 = Wireframe
		; ; 2 = Hidden line
		; ; 3 = Flat shaded
		; ; 4 = Gouraud shaded
		; ; 5 = Flat shaded with wireframe
		; ; 6 = Gouraud shaded with wireframe 
		
		
		(if (and
				mainPViewport
				(= "AcDbViewport" (vla-get-ObjectName mainPViewport))
			)
			(progn
				(princ "Success. We have a main viewport to work with.\n")
			)
			(progn
				(princ "failed to find the main viewport.  quitting...")(princ "\n")
				(quit)
			)
		)
		
		;(setq modelView (vla-get-ModelView mainPViewport))
		(vl-catch-all-apply  'vlax-dump-object (list (vla-get-ModelView mainPViewport)))
		(vl-catch-all-apply  'vlax-dump-object (list (vla-get-SheetView mainPViewport)))
		
		; (if (vla-get-SheetView mainPViewport)
			; (progn
				; (vla-put-Target (vla-get-SheetView mainPViewport) )
			; )
		; )
		
		
		
		(vla-put-Width mainPViewport viewportWidth)
		(vla-put-Height mainPViewport viewportHeight)
		(vla-put-Center mainPViewport (vlax-3d-point viewportCenter))
		(vla-put-Target mainPViewport (vlax-3d-point cameraTarget))
		(vla-put-TwistAngle mainPViewport 0)
		(vla-put-Direction mainPViewport (vlax-3d-point cameraDirection))
		(vla-put-CustomScale mainPViewport viewportScale)
		(vla-put-UCSPerViewport mainPViewport :vlax-false)
		(vla-put-UCSIconOn mainPViewport :vlax-false)
		;;(vla-put-VisualStyle mainPViewport viewportVisualStyle)
		
		;(vla-put-ShadePlot mainPViewport viewportShadePlot)
		;(vla-put-DisplayLocked mainPViewport :vlax-false)
	)
)




(setq numRows 4)
(setq numColumns 3)
(setq reportTable 
	(vla-AddTable (vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
		(vlax-3D-point (list 0 0 0)) ;;insertion point
		numRows ;;numRows
		numColumns ;;numColumns
		0.01 ;; rowHeight
		2 ;; column width
	)
)
(foreach rowIndex (range (- numRows 1) )
	(foreach columnIndex (range (- numColumns 1))
	(setq 	gridVisibilityData			(mapcar 
					'(lambda (x) 
						if(
							(= 
								:vlax-true 
								(vla-GetCellGridVisibility reportTable 
									rowIndex
									columnIndex
									x
								)
							)
							"1"
							"0"
						)
					)
					(list acLeftMask acTopMask acRightMaskacBottomMask)
				)
	   )
	  (princ gridVisibilityData)


		(vla-setText reportTable 
			rowIndex
			columnIndex
			(strcat 
				(strcat "ahoy " (itoa rowIndex) "-" (itoa columnIndex) "\n")
			)
		)
		
	)
)
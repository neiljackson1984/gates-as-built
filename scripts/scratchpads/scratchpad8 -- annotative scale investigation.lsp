(vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	(vlax-for entity blockDefinition
		(setq isAnnotative 
			(vl-catch-all-apply 'getpropertyvalue (list (vlax-vla-object->ename entity) "Annotative"))
		)
		
		(if (vl-catch-all-error-p isAnnotative) 
			(progn
				(setq annotativePropertyIsAvailable nil)
			)
			(progn
				(setq annotativePropertyIsAvailable T)
			)
		)

		
		(if 
			(and annotativePropertyIsAvailable (/= 0 isAnnotative))
			(progn
				(princ "found an annotative object: ")(princ (vla-get-ObjectName entity))
				(if (= "AcDbBlockReference" (vla-get-ObjectName entity))
					(progn
						(princ " (")
							(princ (vla-get-Name entity))
							(if (/= (vla-get-Name entity) (vla-get-EffectiveName entity))
								(progn
									(princ " (")
										(vla-get-EffectiveName entity)
									(princ ") ")
								)
							)
						(princ ") ")
					)
				)
				(princ " in ")(princ (vla-get-Name blockDefinition)) (princ " (") (princ (getEffectiveNameOfBlockDefinition blockDefinition)) (princ ") ")(princ "\n")
			)
		)
	)

)

(getpropertyvalue (car (entsel)) "Annotative")

		(setq isAnnotative 
			(vl-catch-all-apply 'getpropertyvalue (list (car (entsel)) "Annotative"))
		)
		(princ isAnnotative)
		(princ (/= 0 isAnnotative)

		(setq isAnnotative 
			(vl-catch-all-apply 'getpropertyvalue (list (car (entsel)) "Annotative"))
		)		
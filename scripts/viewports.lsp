(defun getMainPViewport (document /
	returnValue
	initialActiveSpace
	initialMSpace
	wholeSheetViewport
	)
	(setq initialActiveSpace (vla-get-ActiveSpace document))
	(setq initialMSpace (vla-get-MSpace document))
	(vla-put-ActiveSpace document acPaperSpace)
	(vla-put-MSpace document :vlax-false)
	(setq wholeSheetViewport (vla-get-ActivePViewport document))
	(vlax-for entity (vla-get-PaperSpace document)
		(if 
			(and
				(= "AcDbViewport" (vla-get-ObjectName entity))
				(/= (vla-get-Handle entity) (vla-get-Handle wholeSheetViewport))
			)
			(progn
				;;(princ "found a candidate.\n")
				(setq returnValue entity)
			)
		)
	)
	
	(vl-catch-all-apply 'vla-put-ActiveSpace (list document initialActiveSpace))
	(vl-catch-all-apply 'vla-put-MSpace (list document initialMSpace))
	returnValue
)

;;this function returns the block reference pointed to by pViewport.LabelBlockID, if it exists, or
;; else returns the first block reference in the owning space of pVewport that refers to the 
;; block definition named "viewLabel" (i.e. has EffectiveName "viewLabel".)
(defun getLabelBlockReferenceOfPViewport( pViewport /
		labelBlockReference
		returnValue
		entity
	)
		
	;;retrieve the label block reference if any (nil if not)
	(setq labelBlockReference
		(vl-catch-all-apply 'vla-ObjectIDToObject 
			(list 
				(vla-get-Document pViewport) 
				(vl-catch-all-apply 'vla-get-LabelBlockID (list pViewport))
			)
		)
	)
	(if (vl-catch-all-error-p labelBlockReference) (setq labelBlockReference nil))
	(if (not labelBlockReference)
		(progn
			;; look for the first block reference in the owning space of pViewport that has EffectiveName "viewLabel"
			(vlax-for entity (vla-ObjectIDToObject (vla-get-Document pViewport) (vla-get-OwnerID pViewport))
				(if
					(and
						(not labelBlockReference)
						(= "AcDbBlockReference" (vla-get-ObjectName entity))
						(= "viewLabel" (vla-get-EffectiveName entity))
					)
					(progn
						(setq labelBlockReference entity)
					)
				)				
			)
		)
	)
	(setq returnValue labelBlockReference)
	returnValue
)


;;just like the (nth) function, the n is zero-based.
;; this function gets the nth value of the given dxf typecode in the given dxfList
(defun getNthValueOfType (dxfList typeCode n /
	)
	(cdr 
		(nth n 
			(vl-remove-if-not 
				'(lambda (x)
					(= (car x) typeCode)
				)
				dxfList
			) 
		)
	)
)

(defun getIndexOfNthValueOfType (dxfList typeCode n  /
	i
	numberOfMatchesSeen
	)
	(setq i 0)
	(setq numberOfMatchesSeen 0)
	(while (< i (length dxfList))
		(if (not indexOfNthValueOfType)
			(if (= (car (nth i dxfList)) typeCode) 
				(progn
					(if (= numberOfMatchesSeen n) (progn  (setq indexOfNthValueOfType i)))
					(plusPlus 'numberOfMatchesSeen)
				)
			)
		)
		(plusPlus 'i)
	)
	indexOfNthValueOfType
)


;; this function does not modify the value passed in, but instead returns a brand new list.  (i.e. it does not expect the value passed in for dxfList to be a symbol)
(defun setNthValueOfType (dxfList typeCode n newValue /
	modifiedElement
	modifiedDxfList
	indexOfNthValueOfType
	)
	
	(setq modifiedElement (cons typeCode newValue))
	(setq indexOfNthValueOfType (getIndexOfNthValueOfType dxfList typeCode n))
	(setq modifiedDxfList (list))
	
	(setq i 0)
	(while (< i (length dxfList))
		(if (= i indexOfNthValueOfType)
			(appendTo 'modifiedDxfList modifiedElement)
			(appendTo 'modifiedDxfList (nth i dxfList))
		)
		(plusPlus 'i)
	)
	modifiedDxfList
)

;; see https://www.autodesk.com/techpubs/autocad/acadr14/dxf/viewport_al_u05_c.htm

(setq xDataGroupCodeForInteger 1070)
(setq xDataGroupCodeForReal 1040)
(setq xDataGroupCodeFor3dPoint 1010)
(setq xDataGroupCodeForControlString 1002)
(setq pViewportXdataFieldMap
	(list 
		(cons "viewTwistAngle" (cons xDataGroupCodeForReal 0))  ;;the viewTwistAngle is the zeroeth (using zero-based indexes) real in the xdata list
		(cons "viewHeight" (cons xDataGroupCodeForReal 1))     ;;the viewHeight is the first (using zero-based indexes) real in the xdata list
		(cons "viewCenterPointX" (cons xDataGroupCodeForReal 2))    
		(cons "viewCenterPointY" (cons xDataGroupCodeForReal 3))    
		(cons "perspectiveLensLength" (cons xDataGroupCodeForReal 4))    
		(cons "frontClipPlaneZ" (cons xDataGroupCodeForReal 5))    
		(cons "backClipPlaneZ" (cons xDataGroupCodeForReal 6))    
		(cons "viewTargetPoint" (cons xDataGroupCodeFor3dPoint 0))    
		(cons "viewDirectionVector" (cons xDataGroupCodeFor3dPoint 1))    
	)
)

(defun getViewportViewInfon (viewport nameOfInfon /
	)
	(getNthValueOfType (gc:GetXdata  viewport "ACAD") 
		(car (cdr (assoc nameOfInfon pViewportXdataFieldMap))) 
		(cdr (cdr (assoc nameOfInfon pViewportXdataFieldMap)))
	)
)

(defun setViewportViewInfon (viewport nameOfInfon newValue / 
	modifiedXData
	)
	(setq modifiedXData 
		(setNthValueOfType (gc:GetXdata  viewport "ACAD") 
			(car (cdr (assoc nameOfInfon pViewportXdataFieldMap))) 
			(cdr (cdr (assoc nameOfInfon pViewportXdataFieldMap))) 
			newValue
		)
	)
	(gc:SetXdata viewport modifiedXData	)
	newValue
)





(defun getViewportLowerLeft (viewport)
	(mapcar '+
		(gc:VariantToLispData (vla-get-Center mainPViewport))
		(list 
			(- (/ (vla-get-Width mainPViewport) 2 ))
			(- (/ (vla-get-Height mainPViewport) 2 ))
			0
		)
	)	
)






(defun reportOnPViewport (viewPort /
	doc
	)
	(setq doc (vla-get-Document viewPort))
	
	(princ "===========================\n")
	(princ "AcDbViewport object: ")(princ "\n")
	(princ "\t\t\t")(princ "owner: ")(princ (vla-get-Name (vla-ObjectIDToObject doc (vla-get-OwnerID  viewPort))))(princ "\n")
	(princ "\t\t\t")(princ "handle: ")(princ (vla-get-Handle  viewPort))(princ "\n")
	(princ "\t\t\t")(princ "LabelBlockId: ")(princ (vl-catch-all-apply 'vla-get-LabelBlockId (list   viewPort)))(princ "\n")
	; (princ "\t\t\t")(princ "ObjectId: ")(princ (vla-get-ObjectId  viewPort))(princ "\n")
	(princ "\t\t\t")(princ "width: ")(princ (vla-get-Width  viewPort))(princ "\n")
	(princ "\t\t\t")(princ "height: ")(princ (vla-get-Height  viewPort))(princ "\n")
	(princ "\t\t\t")(princ "center: ")(princ (gc:VariantToLispData (vla-get-Center  viewPort)))(princ "\n")
	(princ "\t\t\t")(princ "target: ")(princ (gc:VariantToLispData (vla-get-Target  viewPort)))(princ "\n")
	(princ "\t\t\t")(princ "direction: ")(princ (gc:VariantToLispData (vla-get-Direction  viewPort)))(princ "\n")
	(princ "\t\t\t")(princ "visualStyle: ")(princ (vla-get-VisualStyle  viewPort))(princ "\n")
	(princ "\t\t\t")(princ "hasSheetView: ")(princ (vla-get-HasSheetView  viewPort))(princ "\n")
	(princ "\t\t\t")(princ "CustomScale: ")(princ (vla-get-CustomScale  viewPort))(princ "\n")
	(princ "\t\t\t")(princ "ViewHeight: ")(princ (getpropertyvalue (vlax-vla-object->ename viewport) "ViewHeight"))(princ "\n")
	; (princ "\t\t\t")(princ "modelView: ")(princ (vla-get-ModelView  viewPort))(princ "\n")
	; (princ "\t\t\t")(princ "sheetView: ")(princ (vla-get-SheetView  viewPort))(princ "\n")
	; (princ "\t\t\t")(princ "HasExtensionDictionary: ")(princ (vla-get-HasExtensionDictionary  viewPort))(princ "\n")
	; (princ "\t\t\t")(princ "names in extension dictionary: ")(vl-catch-all-apply 'princ (keys (vla-GetExtensionDictionary  viewPort)))(princ "\n")
	;;(princ "\t\t\t")(princ "\"ACAD\" xdata: ")(princ (gc:GetXdata  viewPort "ACAD"))(princ "\n")
	(princ "\t\t\t")(princ "ShadePlot: ")
		(cond
			((= (vla-get-ShadePlot  viewPort) acShadePlotAsDisplayed)  (princ "acShadePlotAsDisplayed"))
			((= (vla-get-ShadePlot  viewPort) acShadePlotHidden)  (princ "acShadePlotHidden"))
			((= (vla-get-ShadePlot  viewPort) acShadePlotRendered)  (princ "acShadePlotRendered"))
			((= (vla-get-ShadePlot  viewPort) acShadePlotWireframe)  (princ "acShadePlotWireframe"))
			(T (princ (vla-get-ShadePlot  viewPort)))
		)
		;;(princ " (")(princ (vla-get-ShadePlot  viewPort))(princ ")")
		(princ "\n")
		
	(foreach item pViewportXdataFieldMap
		(princ "\t\t\t")(princ (car item))(princ ": ")(princ (getViewportViewInfon viewPort (car item)))(princ "\n")
	)
	(princ "\n")
	(princ)
)




; (reportOnPViewport (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))))


;;(vla-Display (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))))
;; (setq mainPViewport (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))))
;;(setViewportViewInfon (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))) "viewTargetPoint" '(0 0 0))
;;(setViewportViewInfon (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))) "viewDirectionVector" '(0 0 1))
; (vla-put-Target mainPViewport (vlax-3d-point (list 0 0 0)))
; (vla-put-direction mainPViewport (vlax-3d-point (list 0 0 1)))
; (vla-Update mainPViewport)


; ; ViewCenter/X
; ; ViewCenter/Y
; ; ViewHeight
; ; ViewTarget/X
; ; ViewTarget/Y
; ; ViewTarget/Z

;(dumpallproperties (vlax-vla-object->ename mainPViewport))


; (setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewCenter/X" 0)
; (setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewCenter/Y" 0)
; (setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewHeight" 0)
; (setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewTarget/X" 0)
; (setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewTarget/Y" 0)
; (setpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewTarget/Z" 0)
; (vla-SyncModelView mainPViewport)


; (gc:DxfListToVariants (gc:GetXdata  viewport "ACAD"))
; (gc:DxfListToVariants (gc:GetXdata  (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD") 'xtyp 'xval)
; (princ "xtyp: ")(princ (gc:VariantToLispData xtyp))(princ "\n")
; (princ "xval: ")(princ (gc:VariantToLispData xval))(princ "\n")
; (princ (gc:VariantsToDxfList xtyp xval))
; (princ (setNthValueOfType (gc:GetXdata  (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD") 
			; (car (cdr (assoc "viewHeight" pViewportXdataFieldMap))) 
			; (cdr (cdr (assoc "viewHeight" pViewportXdataFieldMap))) 
			; 1234567
; )
; )

;;(vla-put-Target (getMainPViewport (vla-get-ActiveDocument (vlax-get-acad-object))) (vlax-3d-point (list 1.11 2.22 3.33)))

;; there appears to be a relationship between the following three parameters:
;;	(vla-get-height mainPViewport) 
;;	(vla-get-CustomScale mainPViewport), 
;;	(getpropertyvalue (vlax-vla-object->ename mainPViewport) "ViewHeight") (which is the same as the viewheight in the "ACAD" xdata attached to the viewport)
;; namely:  ViewHeight * customScale = height.
;; the takeaway message is that ViewHeight does not have anything to do with the height above the ground from which we are viewing the object. Rather, it has more to do with the height of the view on the page. 	
	
(princ)

